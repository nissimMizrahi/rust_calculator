use regex::Regex;
use std::collections::BTreeMap;

pub fn calc_equation(line: &String) -> Result<f64, String>
{
    let mut ret_str = line.replace(" ", "");

    let brackets_detector_query = r"([(]([^()]+)[)])";
    let brackets_detector =  Regex::new(&brackets_detector_query[..]).unwrap();

    loop
    {
        if !brackets_detector.is_match(&ret_str[..])
        {
            match calc_inner(&ret_str[..])
            {
                Ok(num) => return Ok(num),
                Err(e) => return Err(e),
            };
        }

        for cap in brackets_detector.captures_iter(&ret_str.clone()[..]) 
        {
            let whole_part = &cap[1];
            let equation = &cap[2];

            let equation_res = match calc_inner(equation)
            {
                Ok(num) => num.to_string(),
                Err(e) => return Err(e),
            };

            ret_str = ret_str.replace(whole_part, &equation_res[..]);       
        }
    }
    
}

fn calc_action(
    line: &str, 
    funcs: &BTreeMap<char, Box<Fn(f64, f64) -> (f64)>>,
    action: char)
     -> String
{
    let mut ret_str = line.to_string();

    let number_regex = r"(\d+\.\d+|\d+)";
    let mut action_detector_query = format!(r"{}[{}]{}",number_regex, action, number_regex);
    let action_detector =  Regex::new(&action_detector_query[..]).unwrap_or_else(|_e|
    {
        action_detector_query = format!(r"{}\{}{}",number_regex, action, number_regex);
        return Regex::new(&action_detector_query[..]).unwrap();
    });

    loop
    {
        if !action_detector.is_match(&ret_str[..])
        {
            return ret_str;
        }

        for cap in action_detector.captures_iter(&ret_str.clone()[..]) 
        {
            let num1: f64 = (&cap[1]).parse().unwrap();
            let num2: f64 = (&cap[2]).parse().unwrap();

            let result_str = funcs[&action](num1, num2).to_string();
            let fmt = format!("{}{}{}", num1, action, num2);

            ret_str = ret_str.replace(&fmt[..], &result_str[..]);       
        }
    }

}

fn pow(a: f64, b: f64) -> f64 {return a.powf(b);}
fn mul(a: f64, b: f64) -> f64 {return a * b;}
fn div(a: f64, b: f64) -> f64 {return a / b;}
fn add(a: f64, b: f64) -> f64 {return a + b;}
fn sub(a: f64, b: f64) -> f64 {return a - b;}

fn calc_inner(line: &str) -> Result<f64, String>
{   
    let mut actions_to_functions: BTreeMap<char, Box<Fn(f64, f64) -> (f64)>> = BTreeMap::new();

    actions_to_functions.insert('^', Box::new(pow));
    actions_to_functions.insert('*', Box::new(mul));
    actions_to_functions.insert('/', Box::new(div));
    actions_to_functions.insert('+', Box::new(add));
    actions_to_functions.insert('-', Box::new(sub));


    
    let actions = ['^', '*', '/', '+', '-'];
    let mut ret_str = line.to_string();

    for action in actions.iter()
    {
        ret_str = calc_action(&ret_str[..], &actions_to_functions, *action);
    }
    

    match ret_str.parse()
    {
        Ok(ret) => return Ok(ret),
        Err(e) => return Err(format!("cant parse {}\n{}", ret_str, e)),
    }
    
}