use std::io::{self, BufRead};
mod calculator;

fn read_line() -> String
{
    let stdin = io::stdin();
    return stdin.lock().lines().next().unwrap().unwrap();
}

fn run_calculator()
{
    loop
    {
        let line = read_line();
    
        match calculator::calc_equation(&line)
        {
            Ok(res) => println!("{}", res),
            Err(e) => println!("{}", e),
        }
    }
}

fn main() 
{
    run_calculator();
}
